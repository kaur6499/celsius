package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
//Taranpreet kaur
class CelsiusTest {

	@Test
	public void testfromFahrenheitRegular() {
		int temp=Celsius.fromFahrenheit(50);
		assertTrue("Regular", temp==10 );
		
	}
	@Test
	public void testfromFahrenheitBoundaryIn() {
		int temp=Celsius.fromFahrenheit(32);
		assertTrue("Regular", temp==0 );

	}

	@Test
	public void testfromFahrenheitBoundaryOut() {
		int temp=Celsius.fromFahrenheit(-1);
		assertFalse("Regular", temp==0 );
	}

	@Test
	public void testfromFahrenheitExceptional() {
		int temp=Celsius.fromFahrenheit(-4);
		assertFalse("Regular", temp==0 );;
	}


}
